package io.piveau.consus.exporting.hub.ldes;

import io.piveau.rdf.Piveau;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.apache.jena.query.Dataset;
import org.apache.jena.riot.Lang;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(VertxExtension.class)
class EventTest {

    @Test
    void testEvent(Vertx vertx, VertxTestContext testContext) {
        vertx.fileSystem().readFile("example-activity.trig")
                .map(Buffer::toString)
                .onSuccess(data -> {
                    Dataset dataset = Piveau.toDataset(data.getBytes(), Lang.TRIG);
                    Event event = Event.createFrom(dataset);
                    testContext.verify(() -> {
                        Assertions.assertEquals(LDES.Update, event.activityType());
                        testContext.completeNow();
                    });
                })
                .onFailure(testContext::failNow);
    }

}

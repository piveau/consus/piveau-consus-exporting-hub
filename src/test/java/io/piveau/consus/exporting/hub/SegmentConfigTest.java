package io.piveau.consus.exporting.hub;

import io.vertx.core.json.JsonObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SegmentConfigTest {
    @Test
    public void test() {
        String address = "https://example.com:9090/repo";
        String apiKey = "testApiKey";

        JsonObject configJson = new JsonObject()
                .put("hub", new JsonObject()
                        .put("endpoint", new JsonObject()
                                .put("address", address)
                                .put("apiKey", apiKey)));

        SegmentConfig config = SegmentConfig.create(configJson);
        Assertions.assertNotNull(config);
        Assertions.assertEquals(address, config.getAddress("anotherAddress"));
        Assertions.assertEquals(apiKey, config.getApiKey("anotherKey"));
    }

    @Test
    public void testDefault() {
        String address = "https://example.com:9090/repo";
        String apiKey = "testApiKey";

        JsonObject configJson = new JsonObject();

        SegmentConfig config = SegmentConfig.create(configJson);
        Assertions.assertNotNull(config);
        Assertions.assertEquals(address, config.getAddress(address));
        Assertions.assertEquals(apiKey, config.getApiKey(apiKey));
    }
}

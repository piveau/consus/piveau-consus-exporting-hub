package io.piveau.consus.exporting.hub.handler;

import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.SpanKind;
import io.opentelemetry.context.Scope;
import io.piveau.consus.exporting.hub.SegmentConfig;
import io.piveau.consus.exporting.hub.repo.HubRepo;
import io.piveau.consus.exporting.hub.repo.clients.CatalogueClient;
import io.piveau.pipe.PipeContext;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class IdentifierListContent extends ContentBase {

    protected IdentifierListContent(SegmentConfig config, HubRepo hubRepo) {
        super(config, hubRepo);
    }

    @Override
    public void handle(PipeContext pipeContext) {
        Span span = hubRepo.getTracer().span("deleteIdentifiers", SpanKind.CLIENT);

        String catalogueId = pipeContext.getDataInfo().getString("catalogue", "");

        CatalogueClient catalogueClient = hubRepo.catalogueClient(catalogueId, address, apiKey);

        Scope scope = span.makeCurrent();
        catalogueClient.listIdentifiers()
                .compose(targetIds -> {
                    Set<String> sourceIds = new JsonArray(pipeContext.getStringData()).stream().map(Object::toString).collect(Collectors.toSet());
                    int targetSize = targetIds.size();
                    targetIds.removeAll(sourceIds);
                    pipeContext.log().info("Source {}, target {}, deleting {} datasets", sourceIds.size(), targetSize, targetIds.size());
                    List<Future<String>> futures = targetIds.stream().map(originalId -> catalogueClient.deleteDataset(originalId)
                                    .onSuccess(result -> {
                                        span.addEvent("deleted");
                                        pipeContext.log().info("Dataset deleted: {}", originalId);
                                    })
                                    .onFailure(cause -> Content.handleHubRepoFailure(cause, pipeContext, span)))
                            .toList();
                    return Future.join(futures);
                })
                .onComplete(as -> {
                    scope.close();
                    span.end();
                });
    }

}

package io.piveau.consus.exporting.hub.repo;

import io.vertx.core.MultiMap;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpResponse;

public class HubRepoException extends RuntimeException {
    private final int code;
    private final transient JsonObject debugInfo;

    public HubRepoException(HttpResponse<Buffer> response) {
        super(response.statusMessage());
        code = response.statusCode();

        MultiMap headers = response.headers();
        if (headers.contains(HttpHeaders.CONTENT_TYPE) && headers.get(HttpHeaders.CONTENT_TYPE).contains("application/json")) {
            debugInfo = response.body().toJsonObject();
        } else if (response.body() != null) {
            debugInfo = new JsonObject().put("body", response.body().toString());
        } else {
            debugInfo = new JsonObject();
        }
    }

    public int getCode() {
        return code;
    }
    public JsonObject getDebugInfo() {
        return debugInfo;
    }
}

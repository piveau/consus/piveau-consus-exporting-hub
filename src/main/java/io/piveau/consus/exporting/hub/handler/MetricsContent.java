package io.piveau.consus.exporting.hub.handler;

import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.SpanKind;
import io.opentelemetry.context.Scope;
import io.piveau.consus.exporting.hub.SegmentConfig;
import io.piveau.consus.exporting.hub.repo.HubRepo;
import io.piveau.consus.exporting.hub.repo.clients.MetricsClient;
import io.piveau.pipe.PipeContext;
import io.piveau.telemetry.PiveauSemanticConventions;
import io.vertx.core.json.JsonObject;

public class MetricsContent extends ContentBase {


    protected MetricsContent(SegmentConfig config, HubRepo hubRepo) {
        super(config, hubRepo);
    }

    @Override
    public void handle(PipeContext pipeContext) {
        JsonObject dataInfo = pipeContext.getDataInfo();
        String datasetId = dataInfo.getString("identifier", "");

        Span span = hubRepo.getTracer().span("exportMetrics", SpanKind.CLIENT);
        PiveauSemanticConventions.setPipeResourceAttributes(span, dataInfo);

        MetricsClient metricsClient = hubRepo.metricsClient(address, apiKey);

        Scope scope = span.makeCurrent();
        metricsClient.putMetrics(datasetId, pipeContext.getStringData(), pipeContext.getMimeType())
                .onSuccess(result -> {
                    span.addEvent(result);
                    pipeContext.log().info("Metrics graph {}: {}", result, dataInfo);
                })
                .onFailure(cause -> Content.handleHubRepoFailure(cause, pipeContext, span))
                .onComplete(as -> {
                    scope.close();
                    span.end();
                });

        pipeContext.getPipeManager().freeData();
    }

}

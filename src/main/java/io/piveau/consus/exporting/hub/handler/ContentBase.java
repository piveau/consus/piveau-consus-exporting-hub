package io.piveau.consus.exporting.hub.handler;

import io.piveau.consus.exporting.hub.SegmentConfig;
import io.piveau.consus.exporting.hub.repo.HubRepo;

abstract class ContentBase implements Content {

    protected final String address;
    protected final String apiKey;

    protected final HubRepo hubRepo;

    protected ContentBase(SegmentConfig config, HubRepo hubRepo) {
        this.hubRepo = hubRepo;
        address = config.getAddress(hubRepo.getDefaultAddress());
        apiKey = config.getApiKey(hubRepo.getDefaultApiKey());
    }

}

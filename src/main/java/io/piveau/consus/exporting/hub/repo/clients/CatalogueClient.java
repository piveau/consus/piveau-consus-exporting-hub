package io.piveau.consus.exporting.hub.repo.clients;

import io.piveau.consus.exporting.hub.Constants;
import io.piveau.consus.exporting.hub.repo.HubRepoException;
import io.piveau.rdf.Piveau;
import io.piveau.rdf.RDFMimeTypes;
import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpHeaders;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;

import java.util.Set;
import java.util.stream.Collectors;

public class CatalogueClient {

    public static final String PATH_CATALOGUES = "catalogues";

    private final WebClient client;
    private final String catalogueId;

    private String address;
    private String apiKey;

    private final CircuitBreaker circuitBreaker;

    public CatalogueClient(WebClient client, String catalogueId, CircuitBreaker circuitBreaker) {
        this.client = client;
        this.catalogueId = catalogueId;
        this.circuitBreaker = circuitBreaker;
    }

    public void setAddress(String address, String apiKey) {
        this.address = address;
        this.apiKey = apiKey;
    }

    public Future<String> putDataset(String originalId, String data, String contentType) {
        return circuitBreaker.<HttpResponse<Buffer>>execute(promise ->
                        client.putAbs(address + "/" + PATH_CATALOGUES + "/" + catalogueId + "/datasets/origin")
                                .putHeader(Constants.API_KEY_HEADER, apiKey)
                                .putHeader(HttpHeaders.CONTENT_TYPE.toString(), contentType)
                                .addQueryParam("originalId", originalId)
                                .timeout(360000)
                                .sendBuffer(Buffer.buffer(data))
                                .onComplete(promise))
                .map(response -> {
                    switch (response.statusCode()) {
                        case 200, 204 -> {
                            return "updated";
                        }
                        case 201 -> {
                            return "created";
                        }
                        case 304 -> {
                            return "skipped";
                        }
                        default -> throw new HubRepoException(response);
                    }
                });
    }

    public Future<String> putDataset(String originalId, Model model) {
        String data = Piveau.presentAs(model, Lang.NTRIPLES);
        return putDataset(originalId, data, RDFMimeTypes.NTRIPLES);
    }

    public Future<String> deleteDataset(String originalId) {
        return circuitBreaker.<HttpResponse<Buffer>>execute(promise -> client.deleteAbs(address + "/" + PATH_CATALOGUES + "/" + catalogueId + "/datasets/origin")
                        .putHeader(Constants.API_KEY_HEADER, apiKey)
                        .addQueryParam("originalId", originalId)
                        .timeout(360000)
                        .send()
                        .onComplete(promise)
                )
                .map(response -> {
                    switch (response.statusCode()) {
                        case 200, 202, 204 -> {
                            return "deleted";
                        }
                        default -> throw new HubRepoException(response);
                    }
                });
    }

    public Future<Set<String>> listIdentifiers() {
        return circuitBreaker.<HttpResponse<Buffer>>execute(promise ->
                client.getAbs(address + "/" + PATH_CATALOGUES + "/" + catalogueId + "/datasets")
                        .putHeader(Constants.API_KEY_HEADER, apiKey)
                        .addQueryParam("valueType", "originalIds")
                        .timeout(360000)
                        .send()
                        .onComplete(promise)
        ).map(response -> {
            if (response.statusCode() == 200) {
                return response.body().toJsonArray().stream().map(Object::toString).collect(Collectors.toSet());
            } else {
                throw new HubRepoException(response);
            }
        });
    }
}

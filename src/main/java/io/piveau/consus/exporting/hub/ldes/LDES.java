package io.piveau.consus.exporting.hub.ldes;

import org.apache.jena.rdf.model.*;

public class LDES {
    private LDES() {}

    public static final String NS = "https://www.w3.org/ns/activitystreams#";

    public static final Resource Activity = ResourceFactory.createResource(NS + "Activity");

    public static final Resource Delete = ResourceFactory.createResource(NS + "Delete");
    public static final Resource Update = ResourceFactory.createResource(NS + "Update");
    public static final Resource Create = ResourceFactory.createResource(NS + "Create");

    public static final Property object = ResourceFactory.createProperty(NS, "object");
    public static final Property published = ResourceFactory.createProperty(NS, "published");

}

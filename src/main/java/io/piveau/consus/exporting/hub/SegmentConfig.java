package io.piveau.consus.exporting.hub;

import io.vertx.core.json.JsonObject;
import io.vertx.core.json.pointer.JsonPointer;

public class SegmentConfig {

    private final JsonObject config;

    private SegmentConfig(JsonObject config) {
        this.config = config;
    }

    private static final JsonPointer addressPointer = JsonPointer.from("/hub/endpoint/address");
    private static final JsonPointer apiKeyPointer = JsonPointer.from("/hub/endpoint/apiKey");

    public static SegmentConfig create(JsonObject config) {
        return new SegmentConfig(config);
    }

    public String getAddress(String defaultAddress) {
        return (String) addressPointer.queryJsonOrDefault(config, defaultAddress);
    }

    public String getApiKey(String defaultApiKey) {
        return (String) apiKeyPointer.queryJsonOrDefault(config, defaultApiKey);
    }

}

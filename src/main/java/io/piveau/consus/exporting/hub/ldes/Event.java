package io.piveau.consus.exporting.hub.ldes;

import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.ReasonerRegistry;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.vocabulary.RDF;

public class Event {

    private static final Reasoner reasoner;

    private static final Model model = ModelFactory.createDefaultModel();

    static {
        RDFDataMgr.read(model, Thread.currentThread().getContextClassLoader().getResourceAsStream("activitystreams-owl.rdf"), Lang.TURTLE);
        reasoner = ReasonerRegistry.getRDFSSimpleReasoner().bindSchema(model);
    }

    public static Event createFrom(Dataset dataset) {
        InfModel m = ModelFactory.createInfModel(reasoner, dataset.getDefaultModel());
        Resource activity = m.listSubjectsWithProperty(RDF.type, LDES.Activity).next();
        if (activity != null) {
            return new Event(activity, dataset.getNamedModel(activity));
        } else {
            throw new IllegalArgumentException("No activity found");
        }
    }

    private final Resource activity;
    private final Model objectModel;

    private Event(Resource activity, Model objectModel) {
        this.activity = activity;
        this.objectModel = objectModel;
    }

    public Resource activityType() {
        return activity.getProperty(RDF.type).getObject().asResource();
    }

    public Resource objectResource() {
        return activity.getProperty(LDES.object).getObject().inModel(objectModel).asResource();
    }

    public Model objectModel() {
        return objectModel;
    }

    public String published() {
        return activity.getProperty(LDES.published).getObject().toString();
    }

}

package io.piveau.consus.exporting.hub.repo;

import io.piveau.consus.exporting.hub.repo.clients.CatalogueClient;
import io.piveau.consus.exporting.hub.repo.clients.MetricsClient;
import io.piveau.telemetry.PiveauTelemetry;
import io.piveau.telemetry.PiveauTelemetryTracer;
import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.core.Vertx;
import io.vertx.core.tracing.TracingPolicy;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;

public class HubRepo {

    private final WebClient client;

    private final String defaultAddress;
    private final String defaultApiKey;

    private final PiveauTelemetryTracer tracer = PiveauTelemetry.getTracer("piveau-consus-exporter-hub");

    private final CircuitBreaker circuitBreaker;

    public HubRepo(Vertx vertx, String defaultAddress, String defaultApiKey) {
        client = WebClient.create(vertx, new WebClientOptions()
                .setTracingPolicy(TracingPolicy.PROPAGATE)
                .setKeepAlive(true)
                .setMaxPoolSize(200));

        this.defaultAddress = defaultAddress;
        this.defaultApiKey = defaultApiKey;

        circuitBreaker = CircuitBreaker
                .create("hub-repo", vertx, new CircuitBreakerOptions().setMaxRetries(3).setTimeout(-1))
                .retryPolicy((t, c) -> c * 1000L);
    }

    public CatalogueClient catalogueClient(String catalogueId, String address, String apiKey) {
        CatalogueClient catalogueClient = new CatalogueClient(client, catalogueId, circuitBreaker);
        catalogueClient.setAddress(address != null ? address : defaultAddress, apiKey != null ? apiKey : defaultApiKey);
        return catalogueClient;
    }

    public MetricsClient metricsClient(String address, String apiKey) {
        MetricsClient metricsClient = new MetricsClient(client, circuitBreaker);
        metricsClient.setAddress(address != null ? address : defaultAddress, apiKey != null ? apiKey : defaultApiKey);
        return metricsClient;
    }

    public PiveauTelemetryTracer getTracer() {
        return tracer;
    }

    public String getDefaultAddress() {
        return defaultAddress;
    }

    public String getDefaultApiKey() {
        return defaultApiKey;
    }

}

package io.piveau.consus.exporting.hub.handler;

import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.StatusCode;
import io.piveau.consus.exporting.hub.SegmentConfig;
import io.piveau.consus.exporting.hub.repo.HubRepo;
import io.piveau.consus.exporting.hub.repo.HubRepoException;
import io.piveau.pipe.PipeContext;
import io.vertx.core.json.JsonObject;

public interface Content {

    static void dispatch(PipeContext pipeContext, HubRepo hubRepo) {
        JsonObject dataInfo = pipeContext.getDataInfo();
        String content = dataInfo.getString("content", "");

        SegmentConfig config = SegmentConfig.create(pipeContext.getConfig());

        switch (content) {
            case "metrics" -> new MetricsContent(config, hubRepo).handle(pipeContext);
            case "identifierList" -> new IdentifierListContent(config, hubRepo).handle(pipeContext);
            case "ldEvent" -> new LDESContent(config, hubRepo).handle(pipeContext);
            default -> new MetadataContent(config, hubRepo).handle(pipeContext);
        }
    }

    static void handleHubRepoFailure(Throwable cause, PipeContext pipeContext, Span span) {
        span.setStatus(StatusCode.ERROR);
        if (cause instanceof HubRepoException hubRepoException) {
            span.addEvent("failure");
            pipeContext.log().error("{} - {} ({})", hubRepoException.getCode(), hubRepoException.getMessage(), hubRepoException.getDebugInfo().encode());
        } else {
            span.recordException(cause);
            pipeContext.setFailure(cause);
        }
    }

    void handle(PipeContext pipeContext);

}

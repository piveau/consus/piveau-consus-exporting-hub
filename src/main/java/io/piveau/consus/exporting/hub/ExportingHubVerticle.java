package io.piveau.consus.exporting.hub;

import io.piveau.consus.exporting.hub.handler.Content;
import io.piveau.consus.exporting.hub.repo.HubRepo;
import io.piveau.pipe.PipeContext;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class ExportingHubVerticle extends AbstractVerticle {

    public static final String ADDRESS = "io.piveau.pipe.exporting.hub.queue";

    private HubRepo hubRepo;

    @Override
    public void start(Promise<Void> startPromise) {
        vertx.eventBus().consumer(ADDRESS, this::handlePipe);

        ConfigStoreOptions envStoreOptions = new ConfigStoreOptions()
                .setType("env")
                .setConfig(new JsonObject().put("keys", new JsonArray()
                        .add(Constants.ENV_PIVEAU_HUB_REPO_ADDRESS)
                        .add(Constants.ENV_PIVEAU_HUB_REPO_APIKEY)));

        ConfigRetriever.create(vertx, new ConfigRetrieverOptions().addStore(envStoreOptions))
                .getConfig()
                .onSuccess(config -> {
                    String defaultHubAddress = config.getString(Constants.ENV_PIVEAU_HUB_REPO_ADDRESS, Constants.DEFAULT_PIVEAU_HUB_REPO_ADDRESS);
                    String defaultHubApiKey = config.getString(Constants.ENV_PIVEAU_HUB_REPO_APIKEY, Constants.DEFAULT_PIVEAU_HUB_REPO_APIKEY);
                    hubRepo = new HubRepo(vertx, defaultHubAddress, defaultHubApiKey);
                })
                .<Void>mapEmpty()
                .onComplete(startPromise);
    }

    private void handlePipe(Message<PipeContext> message) {
        PipeContext pipeContext = message.body();

        if (pipeContext.log().isTraceEnabled()) {
            pipeContext.log().trace(pipeContext.getPipeManager().prettyPrint());
        }

        Content.dispatch(pipeContext, hubRepo);
    }

}

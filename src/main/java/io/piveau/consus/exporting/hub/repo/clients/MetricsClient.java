package io.piveau.consus.exporting.hub.repo.clients;

import io.piveau.consus.exporting.hub.Constants;
import io.piveau.consus.exporting.hub.repo.HubRepoException;
import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpHeaders;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;

public class MetricsClient {

    private final WebClient client;

    private String address;
    private String apiKey;

    private final CircuitBreaker circuitBreaker;

    public MetricsClient(WebClient client, CircuitBreaker circuitBreaker) {
        this.client = client;
        this.circuitBreaker = circuitBreaker;
    }

    public void setAddress(String address, String apiKey) {
        this.address = address;
        this.apiKey = apiKey;
    }

    public Future<String> putMetrics(String datasetId, String data, String contentType) {
        return circuitBreaker.<HttpResponse<Buffer>>execute(promise ->
                        client.putAbs(address + "/datasets/" + datasetId + "/metrics")
                                .putHeader(Constants.API_KEY_HEADER, apiKey)
                                .putHeader(HttpHeaders.CONTENT_TYPE.toString(), contentType)
                                .timeout(360000)
                                .sendBuffer(Buffer.buffer(data))
                                .onComplete(promise))
                .map(response -> {
                    switch (response.statusCode()) {
                        case 200, 204 -> {
                            return "updated";
                        }
                        case 201 -> {
                            return "created";
                        }
                        default -> throw new HubRepoException(response);
                    }
                });
    }

}

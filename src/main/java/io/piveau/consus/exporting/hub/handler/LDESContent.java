package io.piveau.consus.exporting.hub.handler;

import io.piveau.consus.exporting.hub.SegmentConfig;
import io.piveau.consus.exporting.hub.ldes.Event;
import io.piveau.consus.exporting.hub.ldes.LDES;
import io.piveau.consus.exporting.hub.repo.HubRepo;
import io.piveau.consus.exporting.hub.repo.clients.CatalogueClient;
import io.piveau.pipe.PipeContext;
import io.piveau.rdf.Piveau;
import io.vertx.core.json.JsonObject;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.RDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LDESContent extends ContentBase {

    private final Logger log = LoggerFactory.getLogger(getClass());

    protected LDESContent(SegmentConfig config, HubRepo hubRepo) {
        super(config, hubRepo);
    }

    @Override
    public void handle(PipeContext pipeContext) {
        String data = pipeContext.getStringData();
        String mimeType = pipeContext.getMimeType();

        try {
            Dataset dataset = Piveau.toDataset(data.getBytes(), Piveau.asRdfLang(mimeType));
            Event event = Event.createFrom(dataset);

            JsonObject dataInfo = pipeContext.getDataInfo();
            String catalogueId = dataInfo.getString("catalogue", "");

            CatalogueClient catalogueClient = hubRepo.catalogueClient(catalogueId, address, apiKey);

            Resource activityType = event.activityType();
            if (activityType.equals(LDES.Delete)) {
                catalogueClient.deleteDataset(event.objectResource().getURI());
            } else if (activityType.equals(LDES.Create) || activityType.equals(LDES.Update)) {

                if (event.objectResource().hasProperty(RDF.type, DCAT.Dataset)) {
                    pipeContext.log().info("{} event for {}", event.activityType().getLocalName(), event.objectResource().getURI());
                    if (pipeContext.log().isDebugEnabled()) {
                        pipeContext.log().debug(Piveau.presentAs(dataset, Lang.TRIG));
                    }
                    catalogueClient.putDataset(event.objectResource().getURI(), event.objectModel());
                } else if (event.objectResource().hasProperty(RDF.type, DCAT.Distribution)) {
                    pipeContext.log().warn("Object type is dcat:Distribution");
                } else {
                    pipeContext.log().warn("Object type is not dcat:Dataset");
                }

            } else {
                pipeContext.log().warn("Unsupported activity type: {}", activityType.getURI());
            }
        } catch (Exception e) {
            pipeContext.log().error(data, e);
        }
    }

}

package io.piveau.consus.exporting.hub;

public class Constants {

    private Constants() {}

    static final String ENV_PIVEAU_HUB_REPO_ADDRESS = "PIVEAU_HUB_ADDRESS";
    static final String ENV_PIVEAU_HUB_REPO_APIKEY = "PIVEAU_HUB_APIKEY";

    static final String DEFAULT_PIVEAU_HUB_REPO_ADDRESS = "http://piveau-hub-repo:8080";
    static final String DEFAULT_PIVEAU_HUB_REPO_APIKEY = "apiKey";

    public static final String API_KEY_HEADER = "X-API-Key";

}

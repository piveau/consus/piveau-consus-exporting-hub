package io.piveau.consus.exporting.hub.handler;

import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.SpanKind;
import io.opentelemetry.context.Scope;
import io.piveau.consus.exporting.hub.SegmentConfig;
import io.piveau.consus.exporting.hub.repo.HubRepo;
import io.piveau.consus.exporting.hub.repo.clients.CatalogueClient;
import io.piveau.pipe.PipeContext;
import io.piveau.telemetry.PiveauSemanticConventions;
import io.vertx.core.json.JsonObject;

public class MetadataContent extends ContentBase {

    protected MetadataContent(SegmentConfig config, HubRepo hubRepo) {
        super(config, hubRepo);
    }

    @Override
    public void handle(PipeContext pipeContext) {

        JsonObject dataInfo = pipeContext.getDataInfo();

        String catalogueId = dataInfo.getString("catalogue", "");
        String originalId = dataInfo.getString("identifier", "");

        Span span = hubRepo.getTracer().span("exportMetadata", SpanKind.CLIENT);
        PiveauSemanticConventions.setPipeResourceAttributes(span, dataInfo);

        CatalogueClient catalogueClient = hubRepo.catalogueClient(catalogueId, address, apiKey);

        Scope scope = span.makeCurrent();
        catalogueClient.putDataset(originalId, pipeContext.getStringData(), pipeContext.getMimeType())
                .onSuccess(result -> {
                    span.addEvent(result);
                    pipeContext.log().info("Dataset {}: {}", result, dataInfo);
                })
                .onFailure(cause -> Content.handleHubRepoFailure(cause, pipeContext, span))
                .onComplete(as -> {
                    scope.close();
                    span.end();
                });

        pipeContext.getPipeManager().freeData();
    }
}

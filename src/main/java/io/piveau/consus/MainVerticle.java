package io.piveau.consus;

import io.piveau.consus.exporting.hub.ExportingHubVerticle;
import io.piveau.pipe.connector.PipeConnector;
import io.piveau.vertx.PiveauVertxLauncher;
import io.vertx.core.*;

import java.util.Arrays;

public class MainVerticle extends AbstractVerticle {

    @Override
    public void start(Promise<Void> startPromise) {
        vertx.deployVerticle(ExportingHubVerticle.class, new DeploymentOptions())
                .compose(id -> PipeConnector.create(vertx))
                .onSuccess(connector -> {
                    connector.publishTo(ExportingHubVerticle.ADDRESS, false);
                    startPromise.complete();
                })
                .onFailure(startPromise::fail);
    }

    public static void main(String[] args) {
        String[] params = Arrays.copyOf(args, args.length + 2);
        params[params.length - 2] = "run";
        params[params.length - 1] = MainVerticle.class.getName();

        PiveauVertxLauncher launcher = new PiveauVertxLauncher();
        launcher.dispatch(params);
    }

}
